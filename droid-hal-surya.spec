%define device surya
%define vendor xiaomi
%define vendor_pretty Xiaomi
%define device_pretty Poco X3 NFC
%define installable_zip 1
%define droid_target_aarch64 1
%define android_version_major 11
%define makefstab_skip_entries /system /vendor /product /system_ext
#define pre_actions /bin/sh setup-sources.sh
%define straggler_files \
/bugreports\
/d\
/sdcard\
%{nil}
%include rpm/dhd/droid-hal-device.inc
